<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// polyfill because this is PHP7
function str_starts_with($haystack, $needle) {
    $lh = strlen($haystack);
    $ln = strlen($needle);
    if ($ln > $lh) return false;
    return substr($haystack, 0, $ln) == $needle;
}

$i = @$_GET["i"];
if (!isset($_GET["i"])) {
    http_response_code(400);
    die("no image url");
}
$c = curl_init($i);
curl_setopt($c, CURLOPT_FOLLOWLOCATION, false);

$clear = false;
$buffer = array();
curl_setopt($c, CURLOPT_HEADERFUNCTION, function($c, $h) {
    global $clear, $buffer;
    if (!$clear) {
        if (str_starts_with(strtolower($h), "content-type:")) {
            $i = explode(" ", str_replace("\r\n", "", $h));
            if (str_starts_with($i[count($i) - 1], "image/")) {
                $clear = true;
                for($j = 0; $j < count($buffer); $j++) {
                    header($buffer[$j]);
                }
                header($h);
            } elseif (str_starts_with($i[count($i) - 1], "video/")) {
                header("Location: /rautafarmi/video-file.png");
                die();
            } else {
                http_response_code(302);
                header("Location: /rautafarmi/404.png");
                die();
            }
        }
        else array_push($buffer, $h);
    }
    else header($h);
    return strlen($h);
});
curl_exec($c);
if (curl_error($c) != "") {
    http_response_code(302);
    header("Location: /rautafarmi/404.png");
}
curl_close($c);
