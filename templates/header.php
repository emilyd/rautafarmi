<!DOCTYPE html>
<html lang="en">
	<head>
		<title>rautafarmi Beta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="icon" href="/rautafarmi/favicon.ico" />
	</head>
	<body>
		<span id="top"></span>
		<div class="navbar">
			<a class="navbutton" href="/rautafarmi/">~</a><a class="navbutton" href="/rautafarmi/index.php?rand=<?php echo rand(); ?>">r</a>
			<div style="float: right;">
				<a class="navbutton" id="floatright" href="#top">↑</a><a class="navbutton" id="floatright" href="#bottom">↓</a>
			</div>
		</div>
		<br>
		<div class="center">
			<table class="logotable">
				<tr class="logotable">
					<!--td class="logotable"><img src="icon.png" alt="rautafarmi logo" width="75px" height="143px"></td-->
					<td class="logotable"><img src="beta-icon.png" alt="beta rautafarmi logo" width="75px" height="157px"></td>
					<td class="logotable"><h1>&nbsp;rautafarmi <i>Beta!</i></h1></td>
				</tr>
			</table>
		</div>
		<hr/>
