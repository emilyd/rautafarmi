<?php header('Content-Type: application/json'); header('Access-Control-Allow-Origin: *');
require "../rautafarmi/creds.php";

function test_input($data) {
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

if (isset($_GET["postID"])) {
	$postID = test_input($_GET["postID"]);
	$result = mysqli_query($mysqli, "SELECT PostID,Username,Time,Message,ImageURL FROM posts WHERE PostID = ".$postID);
} else {
	$postID = "";
	$result = mysqli_query($mysqli, "SELECT PostID,Username,Time,Message,ImageURL FROM posts ORDER BY PostID DESC");
}

$posts = array();
while($res = mysqli_fetch_array($result)) {
	#echo "         {\n";
	#echo '             "postID": '.$res['PostID'].",\n";
	#echo '             "username": "'.$res['Username'].'"'.",\n";
	#echo '             "time": "'.$res['Time'].'"'.",\n";
	$message = str_replace("\r\n","",$res['Message']);
	$message = str_replace("\r","",$res['Message']);
	$message = str_replace("\n","\\n",$res['Message']);
	//$message = str_replace('"','a',$amessage]);
	#echo '             "message": "'.test_input($message).'"'."\n";
	#if($res['PostID'] == "1" || $res['PostID'] == $postID) {
		#echo "         }\n";
	#} else {
		#echo "         },\n";
	#}
        
        array_push($posts, array("postID"=>intval($res["PostID"]), "username"=>$res["Username"], "time"=>$res["Time"], "message"=>str_replace("\r", "", $res["Message"]), "imageURL"=>test_input($res["ImageURL"])));
}

echo json_encode(array("posts"=>$posts), JSON_PRETTY_PRINT);
?>
