<?php include 'templates/header.php'?>

<script>
// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
function convertDate(element) {
    var _date = element.parentElement.getElementsByTagName("noscript")[0].innerText;
    var date = new Date(_date);
    var date_str = date.toLocaleString("en-CA", {
        hourCycle: "h24",
        dateStyle: "short",
        timeStyle: "medium"
    }).replace(",", "");
    element.parentElement.appendChild(document.createTextNode(date_str));
}
// @license-end
</script>

		<form name="message" method="post" action="post.php" class="center">
			<table>
				<tr> 
					<td>username</td>
					<td>
						<input type="text" name="username" /> <i>30 char limit</i>
					</td>
				</tr>
				<tr> 
					<td>message</td>
					<td>
						<textarea rows=8 cols=40 name="message"></textarea>
					</td>
				</tr>
				<tr> 
					<td>image URL</td>
					<td>
						<input type="text" name="imgurl" />
					</td>
				</tr>
				<tr>
					<td>action</td>
					<td>
						<input type="submit" name="send" value="send" />
					</td>
				</tr>
			</table>
		</form>
		<hr/>
		<div>
<?php
require "creds.php";

$result = mysqli_query($mysqli, "SELECT * FROM posts ORDER BY PostID DESC");

while($res = mysqli_fetch_array($result)) {
	echo "<div class='message' id='".$res['PostID']."'>";
	echo "<a class='sharebutton' href='#".$res['PostID']."'>#</a> <span class='id'>".$res['PostID']."</span> ";
	if(empty($res['Username'])) {
		echo "<span class='name'>Anonymous";
	} else {
		echo "<span class='name'>".$res['Username']."";
	}
	echo "</span> ";
	if($_SERVER["HTTP_X_FORWARDED_FOR"] == $res["IP"]) {
		echo "<span class='you'>(you) </span>";
	}
	/*echo "<span id='date'><script>var date = new Date('".$res['Time']."'); document.write(date.toString());</script></span><br>";*/
	echo "<span class='date'><noscript>".$res['Time']."</noscript><script>convertDate(document.currentScript)</script></span><br>";
	/*echo "<span id='date'><script>document.write(new Date('".$res['Time']."').toLocaleString());</script></span><br>";*/
	if(! empty($res['ImageURL'])) {
		echo "<a href='".$res["ImageURL"]."'><img class='image' src='https://donut.gq/rautafarmi/image_proxy.php?i=".htmlspecialchars($res["ImageURL"])."'/></a><br/>";
	}
	echo "<span class='message'>".htmlspecialchars($res['Message'])."</span>";
	echo "</div>";
}
?>
	</div>
<?php include 'templates/footer.php'?>
